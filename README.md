# Second Life Analysis

In this project Prof. Trina Davis and Dr. Enrico Indiogine analyse a dataset created from a survey of the Second Life activity that was part of the KATE project.  The dataset is a CVS file that was converted using LibreOffice from a MS Excel file (xlsx).  The first line contains the column label.