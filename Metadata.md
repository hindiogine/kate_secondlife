# MetaData for the KATE Second Life survey

## File PTSLE1

"Yellow" variables are in bold.

[01] ExternalDataReference
Data type: text
Short name: SRN

[02] Prior to this course I have used other 3D computer programs or games.
Data type: Likert, integer, 1:5, Never (1) - Rarely (2) - Sometimes (3) - Often (4) - All of the time (5)

[03] I am comfortable using technology.
Data type: Likert, integer, 0:10

[04] Second Life familiarity and use at the start.
Data type: Likert, integer, 1:4, Never heard (1) - Heard but never used (2) - Few times (3) - Multiple times (4)

[05] I felt that if I tried hard I would be successful using Second Life.
Data type: Likert, integer, 0:10

[06] I thought I would do as well as or better than other students on SL teaching.
Data type: Likert, integer, 0:10

[07] I am confident that I have the ability to learn teaching strategies in SL.
Data type: Likert, integer, 0:10

[08] I felt I had enough time to complete the exercise.
Data type: Likert, integer, 0:10

[09] I found the exercise useful in helping me to learn more about the middle grade student avatar.
Data type: Likert, integer, 0:10

[10] I appreciate having multiple opportunities to practice in SL.
Data type: Likert, integer, 0:10

[11] Was there something you thought was good about the "Get to Know Your Student"?
Data type: text

[12] Was there anything that you would do to improve the Get to Know Your Student exercise?
Data type: text

[13] I felt I had enough time to complete the exercise.
Data type: Likert, integer, 0:10

[14] I found the exercise useful in allowing me to work one on one with the middle grade student avatar to develop my tutoring teaching skills.
Data type: Likert, integer, 0:10

[15] How did you feel about tutoring the middle grade student?
Data type: Likert, integer, 0:10

[16] Please comment on your response in the above question.
Data type: text

[17] Did the actual tutoring session meet your expectations plans?
Data type: Likert, integer, 0:10

[18] d  Please briefly explain your above answer.
Data type: text

[19] Was there something you thought was good about the one-on-one Tutoring?
Data type: text

[20] Would you change anything in the one on one Tutoring?
Data type: text

## File PTSLE2

In this document is the description of the variables.  We have free fields and Likert scale as well as other type of Data.
The "yellow" variables are considered at the moment the most interesting ones.

[01] ExternalDataReference
Data type: text

[02] Race
Data type: integer, 1:8

Please respond to the following based on your experience “PLANNING or PRACTICING Delivering your SL Lesson with Peers”
[03] I found Practicing Delivering the SL Lesson with Peers valuable?
Data type: integer, 0:10

[04] I found practicing using the SmartPodium useful?
Data type: integer, 0:10

[05] Was there anything that you would do to improve the PRACTICING Delivering your SL Lesson with Peers?
Data type: text

Please respond to the following based on the engagement in the FINAL Problem Solving Lesson in Second Life:
[06] I felt I or my partner had enough time to complete the Problem Solving Lesson.
Data type: integer, 0:10, Likert

[07] I found the Problem Solving Lesson valuable in allowing me (or my partner) to work with the middle grade student avatars to develop teaching skills.
Data type: integer, 0:10

Please respond to the following based on your classroom engagement with the student avatars.
[08] During my lesson, I was able to distinguish between the live and bot avatars.
Data type: integer, 0:10

[09] I enjoyed working with (or observing) the full classroom of live and bot avatars.
Data type: integer, 0:10

[10] I found working with a classroom of live and bot avatar students a good setting to practice my teaching.
Data type: integer, 0:10

[11] The avatars' gestures enhanced the overall classroom engagement (hand-raising, question marks, light bulb).
Data type: integer, 0:10

[12] The avatars verbal responses and questions enhanced the overall classroom engagement.
Data type: integer, 0:10

[13] How did you feel about teaching (or observing) the FINAL Problem Solving Lesson with the middle grade student avatars in Second Life?
I felt comfortable teaching the middle grade student avatars in Second Life.
Data type: integer, 0:10

[14] Please comment or elaborate on your response in the above question.
Data type: text

[15] Did the actual problem solving teaching lesson meet your expectations/plans? Please briefly explain your answer.
Data type: text

[16] Was there something you thought was good about the FINAL “SL Problem Solving Lesson"? If so, please give one or two examples.
Data type: text

[17] Would you change anything if you were given another opportunity? If so, please give one or two examples.
Data type: text

[18] Did your interaction or observation with the middle grade students during the FINAL problem solving lesson lead to a personal insight (mathematical or pedagogical)? Please provide details.
Data type: text

Please respond to the following based on your use of the Virtual Classroom Observation Instrument to review your SL problem solving lesson or others lesson(s).
[19] I found the classroom observation form useful for reviewing the "demonstration" lesson.
Data type: integer, 0:10

[20] I found the observation form useful for reviewing and reflecting on teaching my problem-solving lesson.
Data type: integer, 0:10

[21] I found the classroom observation form useful for reviewing my classmates' problem solving lessons.
Data type: integer, 0:10

[22] How has the following activitiy CHANGED your AWARENESS about diversity issues?
Problem Solving Lesson Presentation in Second Life
Data type: integer, 1:4 (Likert)

[23] One more thing I would like to say about diversity issues is:
Data type: text

[24] How has the following activity CHANGED your KNOWLEDGE ABOUT TEACHING algebra for diversity?
Problem Solving Lesson Presentation in Second Life
Data type: integer, 1:4 (Likert)

[25] One more thing I would like to say about teaching algebra for diversity is:
Data type: text

[26] Please respond to the following based on your OVERALL Second Life experiences and engagement thrughout the semester:
Learning to operate this kind of computer program (virtual environment) was easy for me.
Data type: integer, 0:10

[27] Learning how to use this type of computer program was complicated and difficult for me.
Data type: integer, 0:10

[28] Overall I think this kind of computer program is easy to use.
Data type: integer, 0:10

[29] This type of computer simulation program allowed me to be more responsive and active in the learning process.
Data type: integer, 0:10

[30] This type of computer simulation program allowed me to respond to learners needs while teaching.
Data type: integer, 0:10

[31] Using this kind of computer program as a tool for practicing my classroom teaching, improves/will improve my teaching.
Data type: integer, 0:10

[32] Using this kind of classroom simulation enhances/will enhance the effectiveness of my teaching.
Data type: integer, 0:10

[33] I was able to reflect on how I teach.
Data type: integer, 0:10

[34] I was able to reflect on my own understanding in terms of mathematics teaching.
Data type: integer, 0:10

[35] I was able to reflect on my own understanding in terms of my equity consciousness.
Data type: integer, 0:10

[36] The experience of engaging in exercises and giving my lesson in SL allowed me to engage with middle grade student avatars in a meaningful way.
Data type: integer, 0:10

[37] I had a sense of being PRESENT (or being there) in the virtual learning space, when I participated in the various SL activities.
Data type: integer, 0:10

[38] Engaging in dialogue and questioning with the student avatars enhances/will enhance the effectiveness of my teaching.
Data type: integer, 0:10

[39] After this course, I will be open to using 3-D computer programs, virtual environments, or games in my TEACHING.
Data type: integer, 0:10

[40] After this course, I will be open to using 3-D computer programs, virtual environments, or games, for my LEARNING.
Data type: integer, 0:10

[41] I experienced technical or other issues during one or more of the Second Life exercises.
Data type: integer, 0:10

[42] Please explain, if you experienced technical or other issues [and include the specific exercise(s)].
Data type: text

Indicate all Second Life skills that you were able to develop in the course: General Second Life Skills AND Second Life Teaching Skills:
[43] General Second Life Skills: Create an account and log into Second Life
Data type: integer, 0:10

[44] General Second Life Skills: Visit or access a location in Second Life (e.g. KATE Virtual Classroom)
Data type: integer, 0:10

[45] General Second Life Skills: Complete 8-Station Second Life Training Orientation
Data type: integer, 0:10

[46] General Second Life Skills: Edit avatars appearance or clothing
Data type: integer, 0:10

[47] General Second Life Skills: Walk or sit down with avatar
Data type: integer, 0:10

[48] General Second Life Skills: Engage in text Chat (whole or small group)
Data type: integer, 0:10

[49] General Second Life Skills: Engage in IM (instant messaging) in SL
Data type: integer, 0:10

[50] General Second Life Skills: Watch Videos in SL (e.g. orientation videos)
Data type: integer, 0:10

[51] General Second Life Skills: Use VOICE Chat and other communication tools for teaching or tutoring
Data type: integer, 0:10

[52] General Second Life Skills: Design and advance slides in SL to deliver lesson or content
Data type: integer, 0:10

[53] General Second Life Skills: Use the SmartPodium to work out mathematics problems in SL
Data type: integer, 0:10

[54] General Second Life Skills: Deliver a Problem Solving Lesson using various SL classroom tools
Data type: integer, 0:10

[55] Please provide any additional feedback that you think would be helpful about your experiences this semester. (Optional).
Data type: text
